
---?image=https://uploads-abcopen.s3.amazonaws.com/assets/uploads/242291.jpg

# @color[white](Bioinformatics:)
## @color[white](The dark art of big data)
<br>
@color[white](Jimmy Breen)
<br>
@color[white](Presentation code available: https://bitbucket.org/sahmri_bioinformatics/sahmri_bioinformatics_seminar)

---?image=https://www.weekendnotes.com/im/004/09/img734011.JPG
---?include=main_pres.md
