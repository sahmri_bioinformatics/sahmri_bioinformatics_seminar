

## The important questions...

- What is Bioinformatics and why is it important?
    - No ...seriously... what is Bioinformatics?

- What happens when Bioinformatics goes wrong?

- Who and what is the SAHMRI Bioinformatics Core Facility?

- Bioinformatics in the Adelaide BioMed City Precinct

---

## Who am I?

- Analysed data from Plants, Fungi, Fish, Humans, Mice and Bacteria

- Research Interest: Epigenetic and structural gene regulation (NHMRC) & Placental Genomics (NIH)
    - ARC Discovery in Visual Systems in Sea Snakes

- Researcher in public and private institutions

- Senior lecturer at the University of Adelaide

---

## Who am I?

- Trained in Molecular Biology and Bioinformatics:
    - Worked at the bench, in the field and now on a computer

<img src="https://bitbucket.org/sahmri_bioinformatics/sahmri_bioinformatics_seminar/raw/dd3d2474ba547027169c4ea46af795960522c778/images/armenia_rs.jpg" style="width:125vh">

@size[small](Noravank Monastery, Armenia)

---


### What is Bioinformatics?


@quote[Research, development, or application of computational tools and approaches for exapanding the use of biological ... data, including those to ... organise... analyse or visualize such data](NIH Biomedical Information Science and Technology Initiative Consortium)


@quote[The application of computer technology to management and analysis of biological data](EMBL European Bioinformatics Institute)

---

### What do Bioinformaticians do?

- Analysis of biological datasets
    - Genomics, Transcriptomics (RNAseq), Epigenomics (DNA Methylation, Histone modification, small RNAs), Proteomics, Metabolomics etc
    - Plus metadata ...

- Data custodians (storage, maintenance, accessibility etc)

- Software & computational method development

- Visualisation

- We are NOT statisticians

---

## How 'Big' is 'Big Data'?

<img src="images/petabytes.png" style="width:80vh" align="center">
<br>
@size[small](Denis Bauer & Lynn Langit, CSIRO)
---

## How 'Big' is 'Big Data'?

<img src="images/exabytes.png" style="width:80vh" align="center">
<br>
@size[small](Denis Bauer & Lynn Langit, CSIRO)

---

## How 'Big' is 'Big Data'?

What do we do with all this data?

<img src="images/bigdata.jpg" style="width:60vh" align="center">

---

### Why do we need Bioinformatics?

- We're producing more data that we can analyse

<img src="http://epilepsygenetics.net/wp-content/uploads/2012/06/genome3-1024x723.jpg" style="width:60vh">

- Moore's Law vs Moore's Wall
---


### Why do we need Bioinformatics?

@quote[A big computer, a complex algorithm and a long time does not equal science.](Robert Gentleman)

<img src="images/data_whisperers.png" style="width:55vh">

Trained Bioinformaticians are the data whisperers
---

### The Dark Arts

- Bioinformatics is often poorly understood
    - By both biologists and computer scientists
    - It's a new science

- Diverse field of researchers
    - Programmers, analysts, domain-specific skills

- Software written by Bioinformaticians for Bioinformaticians
    - We often lack formal computer sciences training
    - Serve one purpose, and have limited update cycles
    - Far from user friendly

- The bigger the data, the bigger the mistakes


---

### Big Data Horror Stories

#### Duke University Scandal (Dr Anil Potti)

- Claimed to use gene expression data to predict which cancer treatment would be most effective for a specific patient

- Problems identified by Bioinformaticians Keith Baggerly and Kevin Coombes (MD Anderson Cancer Center, Houston)

- Among errors, an Microsoft Excel spreadsheet mistake

---

### Big Data Horror Stories

<img src="images/excel_names.png" style="width:60vh">

---

### Why is SAHMRI investing in Bioinformatics?

- Provide Bioinformatics consulting for SAHMRI Themes

- Develop collaborations between wet and dry labs

- Computational methods and algorithms

- Fee-for-service work on larger projects
    - Work with the genomics facility

---

### What will the SAHMRI Core-Facility provide?

- Consultation on setting up genomics projects
    - Data storage requirements, personnel etc

- Data analysis for grant applications

- Training and user workshops

- Postgraduate Student supervision

---

### What will the SAHMRI Core-Facility provide?

- Management of important datasets
    - UK Biobank, NIH Epigenomics Roadmap data etc

- Some high performance computing maintenance
    - Bioinformatics programs and pipelines

---

### What sort of research will you be doing?

- Data integration of 'omic' data types

- Using public and SAHMRI data to address different research questions

- High-dimensional data visualisation

- Software and tool development to aid researchers

---

### Expression QTL analysis of RNA-seq Cohorts

<img src="images/qtl_man_plot.png" style="width:120vh">
<br>
@size[small](Breen, Bianco-Miotto, Roberts _et al._ 2018)

---

### Visualisation

<p float="middle">
<img src="images/zebrafish_human2.png" style="width:35vh">
<img src="images/zebrafish_human1.png" style="width:35vh">
</p>

@size[small](Nhi Hin _et al._ "Accelerated brain aging towards transcriptional inversion in a zebrafish model of familial Alzheimer's disease", _bioRxiv_ 2018)

---

### Visualisation

<img src="https://raw.githubusercontent.com/awaisc/motifOverlapR/master/ImagesForReadMe/ComputationalTab.jpg" style="width:80vh">
<br>
@size[small](Awais Choudhry, https://github.com/awaisc/motifOverlapR)

---

### Software and Tool Development

- Christopher Ward, Hien To and Stephen Pederson, 2018, ngsReports (https://github.com/UofABioinformaticsHub/ngsReports)

- Awais Choudhry, Stephen Pederson, Cheryl Shoubridge & Jimmy Breen. (2018). MotifOverlapR: multi-omic transcription factor binding site filtration and visualisation (https://github.com/awaisc/motifOverlapR).

- Pederson S, To H. (2018). strandCheckR: Calculate strandness information of a bam file. R package version 0.99.14. (https://www.bioconductor.org/packages/devel/bioc/html/strandCheckR.html).

---

### Bioinformatics workflows

- Genomics data repository and workflow manager
    - Enables researchers to logon to a web-browser and run 'best practises' pipelines
    - Pipelines written is CWL/WDL (workflow languages) and runs in docker containers
    - Can be run within Adelaide HPC (Phoenix)
    - Results and raw data transferred to cold data storage (200TB)

---

### Bioinformatics workflows

<img src="images/workflow_tool.png" style="width:50vh">

---

## Who are we?

<img src="images/sahmri_core_personel.png" style="width:70vh">

- Part of the Adelaide BioMed City Precinct
- Collaboration with the University of Adelaide Bioinformatics Hub

---

### Adelaide BioMed City

<img src="images/abmc.png" style="width:75vh">

- ABMC Bioinformatics Seminars start September 2018

---

### What is the Bioinformatics Hub?

<img src="images/hub_personel.png" style="width:70vh">

- Collection of staff from across University of Adelaide that conduct Bioinformatics projects
    - Expertise across all Bioinformatics skills

---

### Acknowledgments

- Deb White, David Lynn & Steve Wesselingh (SAHMRI)

- Steve Pederson & Dave Adelson (University of Adelaide)

- James & Diana Ramsay Foundation

- Office of the Deputy Vice-Chancellor for Research (DVCR), University of Adelaide

Feel free to contact the Bioinformatics team (jimmy.breen@sahmri.com) for projects and questions

Thank you!
